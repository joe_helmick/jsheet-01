;##################################################################################################
memdump:
;##################################################################################################
; input		HL has address to start dumping from
; output	text to terminal

rows_to_dump = 12
cols_to_dump = 16
memdump_page_size = 10
char_unprintable = 176; 250

	ld	hl, (memdumpStart)

	call 	sendcrlf
	ld	c, 0			; starting row index
memdump_nextrow:
	ld	b, 0			; starting column index
	call	print_address
memdump_nextbyte:
	ld	a, (hl)			; load A with the byte that HL is pointing to

	push	hl			; save off HL so we can restore later

	ld	d, 0			; load column offset into DE
	ld	e, b
	ld	hl, memdumpCharString	; point HL at the output string
	add	hl, de			; add column offset to HL

	cp	0x20			; compare A to space
	jp	p, checkForDelete	; no? check next option
	ld	(hl), char_unprintable	; yes? output a placeholder character
	jr 	done0034		; bail out here

checkForDelete:
	cp	0x7F			; compare A to delete
	jr	nz, regularCharacter	; no? check next option
	ld	(hl), char_unprintable	; yes? output a placeholder character
	jr 	done0034		; bail out here

regularCharacter:
	ld	(hl), a			; output a regular character

done0034:
	pop	hl			; restore HL and continue with hex dump

	push	hl			; save off HL and BC before bin_to_hex
	push	bc
	call	bin_to_hex
	ld	a, h			; print the high nibble
	rst 8
	ld	a, l			; print the low nibble
	rst 8
	ld	a, ' '			; print a space
	rst 8
	pop	bc			; restore BC and HL now	
	pop	hl
	inc	hl			; move to next address
	inc	b			; move to next column
	ld	a, b			; are we at end of row?
	cp	cols_to_dump
	jr	nz, memdump_nextbyte	; loop and continue if not

	push	hl			; print the char string before going to next line
	ld	hl, memdumpCharString
	call	print
	pop	hl

	call	sendcrlf		; now, send CRLF to terminal, go to next line
	inc	c			; increment the row
	ld	a, c			; are we done printing rows?
	cp	rows_to_dump
	jr	nz, memdump_nextrow	; no? then do next row
	ret				; yes? then return
