ECHO OFF
if exist *.bin ( del *.bin )
if exist jsheet.lis ( attrib -R jsheet.lis )

ECHO ASSEMBLING
D:\devtools\z88dk\z80asm.exe  --cpu=z80 --list --make-bin jsheet.asm
ECHO MAKING
D:\devtools\z88dk\appmake.exe +hex --binfile jsheet.bin --org 0000h
ECHO MAKING LIST FILE READ-ONLY
attrib +R jsheet.lis
ECHO OFF
if exist *.o ( del *.o )
if exist *.err ( del *.err )
if exist *.ihx ( del *.ihx )