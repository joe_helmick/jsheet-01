;------------------------------------------------------------------------------------------
; Print a null-terminated string character by character wherever the cursor is.
; Address to string is in HL.
print:
	ld	a,(hl)
	or	a
	ret	z
	rst 8
	inc	hl
	jr	print
	ret
