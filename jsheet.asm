; Joe's Spreadsheet Program JSHEET
; https://gitlab.com/joe_helmick/jsheet-rom
;
; Serial communications section by (c) Grant Searle
; http://searle.hostei.com/grant/#MyZ80
;
; Assembled using z88dk toolkit.
; https://www.z88dk.org/forum/
;
; note: set tab size to 8 for best alignment
; note: set terminal emulator to display code pagte CP437 for best appearance


ramstart		=	$2000		; first byte of ram; change only if memory changes

;---------------------------------------------------------------------------------------------------
serbufsz		=	32		; serial buffer size
serfulsz		=	24		; serial fullsize
serempsz		=	5		; serial empty size
rts_high		=	$d5		; 115200 bps from 68b50 acia w 1.8232 mhz clk
rts_low			=	$95		; 115200 bps from 68b50 acia w 1.8232 mhz clk
serfull			= 	serbuf + serbufsz
;---------------------------------------------------------------------------------------------------
CR			=	$0d		; carriage return
LF			=	$0a		; linefeed
cls			=	$0c		; clearscreen
esc			=	$1b		; escape
bs			=	$08		; backspace CTRL-H
del			=	$7f		; delete key

inputbufsz		= 	64
inputfunction_null	= 	$00
inputfunction_up	=	$01
inputfunction_dn	=	$02
inputfunction_left	=	$03
inputfunction_right	=	$04
inputfunction_home	=	$05
inputfunction_end	=	$06
inputfunction_pgup	=	$07
inputfunction_pgdn	= 	$08
inputfunction_ins	=	$10
inputfunction_f1	= 	$16
inputfunction_f2	= 	$17
inputfunction_f3	= 	$18
inputfunction_f4	= 	$19
inputfunction_f5	= 	$20
inputfunction_text	=	$FF


; variables located in ram
defvars ramstart
{
	serbuf			ds.b serbufsz
	inputbuf		ds.b inputbufsz

	serinptr		ds.w 1		; the buffer's input pointer
	serrdptr		ds.w 1		; the buffer's read pointer
	serbufused		ds.b 1		; count of bytes used

	inputbufptr		ds.w 1
	inputcharsleft		ds.b 1

	inputfunction		ds.b 1		; hold the input function like PgUp or F3 or whatever

	curLocRow		ds.b 1		; holds current row number as one or two bcd digits
	curLocCol		ds.b 1		; hold current column letter

	memdumpCharString	ds.b 17
	memdumpStart		ds.w 1


	lastvar			ds.b 1		; sentinel value for mfill routine
}
defvars ramstart + 0x80
{
	formulaListHead		ds.b 1		
}

; ###################################################################################################
; ###########################  program execution start here at 0x0000 ###############################
; ###################################################################################################

	; ; zero out all RAM first  DEBUG ONLY, shouldn't be necessary in production
	; ld	hl, ramstart
	; ld	bc, 65536-ramstart
	; ld	a, 0

	; ld	(hl), a
	; ld	d, h
	; ld	e, l
	; inc	de
	; dec	bc
	; ld	a, b
	; or	c
	; ret	z
	; ldir

include	"gsInit.asm"

;###################################################################################################
;#######################     end of initialization routines, start program      ####################
;###################################################################################################

command_msg:		defm	"> ", 0
youtyped_msg:		defm	"you typed: ", 0

include "print.asm"

include "mfill.asm"

include "bin_to_hex.asm"
	
include "memdump.asm"	

include "print_address.asm"

;###################################################################################################
reset_input_buffer:	
;###################################################################################################
	; reset the input buffer pointer to the start of the buffer
	ld	hl, inputbuf
	ld	(inputbufptr), hl
	; null out the input buffer
	ld	bc, inputbufsz
	ld	a, 0
	call	mfill
	; reset the character count, leaving one space at end for a null character
	ld	hl, inputbufsz - 1
	ld	(inputcharsleft), hl
	; null out the input function variable
	ld	a, inputfunction_null
	ld	(inputfunction), a
	ret




;###################################################################################################
print_input_function:
;###################################################################################################
	ld	a, (inputfunction)
	; convert to hexadecimal
	call	bin_to_hex
	; print the high nibble
	ld	a, h
	rst 8
	; print the low nibble
	ld	a, l
	rst 8
	ret




;###################################################################################################
sendcrlf:
;###################################################################################################
	push	af
	ld	a, CR
	rst 8
	ld	a, LF
	rst 8
	pop	af
	ret




;###################################################################################################
bin2bcd99:
; input 	A has value to convert
; output	L has 2 BCDF digits 00-99
;###################################################################################################
	ld	l, $FF
div10loop1:
	inc	l
	sub 	10
	jr	nc, div10loop1
	add	a, 10
	ld	c, a
	ld	a, l
	rlca
	rlca
	rlca
	rlca
	or	c
	ld	l, a
	ret	





;###################################################################################################
printLocPrompt:
;###################################################################################################
	call	sendcrlf
	ld	a, (curLocCol)
	rst 8
	ld	a, (curLocRow)
	call	bin2bcd99
	ld	a, l
	srl	a
	srl	a
	srl	a
	srl	a
	cp	0
	jr	z, leadingzero
	add	'0'
	rst 8
leadingzero:	
	ld	a, l
	and	$0F
	add	'0'
	rst 8
	ld	a, '>'
	rst 8
	ret





;###################################################################################################
; This is the get input loop, or gil.  stay here until some kind of inputfunction-
; creating input is received.  Then drop back to main loop to handle input
gil:
;###################################################################################################
; first get one character from the serial buffer
	call	rxa
; handle escape key
	cp	esc
	jr	z, ansi_esc
; handle backspace key
 	cp	bs
 	jr	z, itsBackSpace
; handle delete key same as backspace for now
 	cp	del
 	jp	z, itsBackSpace
; handle <enter> key
	cp	CR
	jr	z, endentry
; handle regular keys
	push	af
	; prevent buffer overrun
	ld	a, (inputcharsleft)
	cp	a, 0
	jr	z, gil
	; some room left so decrement the count
	dec	a
	ld	(inputcharsleft), a
	; get the character back
	pop	af
	; add the character to the input buffer
	ld	hl, (inputbufptr)
	ld	(hl), a
	; advance the pointer and save new position
	inc	hl
	ld	(inputbufptr), hl
	; echo character so user can see what's been typed
	rst 8
	; return to top of input loop
	jp	gil

;------------------------------------------------
itsBackSpace:
	; prevent buffer underrun
	push	af
	ld	a, (inputcharsleft)
	cp	inputbufsz - 1
	jr	z, gil
	; no underrun, while we have inputCharsleft in A, inc it
	inc	a
	ld	(inputcharsleft), a
	pop	af
	; back up a position in the buffer and put a null there
	ld	hl, (inputbufptr)
	dec	hl
	ld	(inputbufptr), hl
	ld	(hl), 0
	; send a backspace character
	ld	a, bs
	rst 8
	; jump back to top of loop
	jp	gil
;------------------------------------------------
endentry:
	; char was a CR, so add an LF and display them to get newline (for now)
	call	sendcrlf 
	; fill in inputfunction, ready to exit now
	ld	a, inputfunction_text
	ld	(inputfunction), a
	ret
;------------------------------------------------
ansi_esc:
	; character was escape so lets see if a bracket follows
	call	rxa
	cp	'['
	; bail out now if just regular escape key, not followed by bracket
	ret	nz

	; otherwise get at least one more character
	call	rxa

	; test single characters first to get them out of the way
	cp	'A'
	call	z, itsUp
	cp	'B'
	call	z, itsDown
	cp	'D'
	call	z, itsLeft
	cp	'C'
	call	z, itsRight

	; thes next ones have a trailing tilde but we can decode with what we know now
	cp	'4'
	call	z, itsEnd
	cp	'5'
	call	z, itsPgUp
	cp	'6'
	call	z, itsPgDn

	; NOTE several codes start with '2' but we'll just handle this one for the moment
	cp	'2'
	call	z, itsIns

	; several codes start with '1' so begin decoding these now
	cp	'1'
	ret	nz
	
	; to decode further we need another character
	call	rxa
	cp	'~'		; is it a tilde?
	call	z, itsHome
	cp	'1'
	jp	z, itsF1
	cp	'2'
	call	z, itsF2
	cp	'3'
	call	z, itsF3
	cp	'4'
	call	z, itsF4
	cp	'5'
	call	z, itsF5

	; bail out as that's all we know how to decode now
	ret

	itsUp:
		ld	a, inputfunction_up
		ld	(inputfunction), a
		ret
	itsDown:
		ld	a, inputfunction_dn
		ld	(inputfunction), a
		ret
	itsLeft:
		ld	a, inputfunction_left
		ld	(inputfunction), a
		ret
	itsRight:
		ld	a, inputfunction_right
		ld	(inputfunction), a
		ret
	itsEnd:
		ld	a, inputfunction_end
		ld	(inputfunction), a
		ret
	itsPgUp:
		call	rxa			; dispose of trailing ~
		ld	a, inputfunction_pgup
		ld	(inputfunction), a
		ret
	itsPgDn:
		call 	rxa			; dispose of trailing ~
		ld	a, inputfunction_pgdn
		ld	(inputfunction), a
		ret
	itsIns:	
		ld	a, inputfunction_ins
		ld	(inputfunction), a
		ret
	itsF1:
		call	rxa			; dispose of trailing ~
		ld	a, inputfunction_f1
		ld	(inputfunction), a
		ret
	itsF2:
		ld	a, inputfunction_f2
		ld	(inputfunction), a
		ret
	itsF3:
		ld	a, inputfunction_f3
		ld	(inputfunction), a
		ret
	itsF4:
		ld	a, inputfunction_f4
		ld	(inputfunction), a
		ret
	itsF5:
		ld	a, inputfunction_f5
		ld	(inputfunction), a
		ret
	itsHome:
		ld	a, inputfunction_home
		ld	(inputfunction), a
		ret


;###################################################################################################
formulaSlot1:
;###################################################################################################
	ld	hl, (formulaListHead)
	ld	a, (formulaListHead)
	cp	0
	jz	findFormulaList1Found

formulaSlot1Found:
	ret	; HL points to next open spot on list




;###################################################################################################
;###########     main progam starts here      ######################################################
;###################################################################################################

main1:
	
	ld	sp, $0			; load the stack pointer at top of ram
	
	call	reset_input_buffer	; clear the input buffer

	ld	a, 0			; DEBUG initialize memdump string
	ld	bc, 17
	ld	hl, memdumpCharString
	call	mfill

	ld	hl, ramstart		; DEBUG initialize memdump address
	ld	(memdumpStart), hl

	ld	a, 'A'			; initialize location row and column
	ld	(curLocCol), a
	ld	a, $01
	ld	(curLocRow), a


; This is the top of the main "do while true" loop, no escape from this loop.
mainloop:

	; clear out the input function
	ld	a, inputfunction_null
	ld	(inputfunction), a

	push	af
	call	printLocPrompt
	pop	af

	; call main get input loop
	call 	gil

	; load the saved inputfunction
 	ld	a, (inputfunction)

	; handle F1 (memdump)
	cp	inputfunction_f1
	call	z, command_memdump

	; handle text function
	cp	inputfunction_text
	call	z, command_print

	; handle page up
	cp	inputfunction_pgup
	call	z, command_pgup

	; handle page down
	cp	inputfunction_pgdn
	call	z, command_pgdn

	cp	inputfunction_left
	push	af
	call	z, command_left
	pop	af

	cp	inputfunction_right
	push	af
	call	z, command_right
	pop	af

	cp	inputfunction_up
	push	af
	call	z, command_up
	pop	af

	cp	inputfunction_dn
	push	af
	call	z, command_down
	pop	af

	call	reset_input_buffer	; reset input buffer as last thing before looping

	jr	mainloop
	;----------------------------------------

command_up:
	ld	a, (curLocRow)
	cp	1
	ret	z
	dec	a
	ld	(curLocRow), a
	ret

command_down:
	ld	a, (curLocRow)
	cp	99
	ret	z
	inc	a
	ld	(curLocRow), a
	ret

command_left:
	ld	a, (curLocCol)
	cp	'A'
	ret	z
	dec	a
	ld	(curLocCol), a
	ret

command_right:
	ld	a, (curLocCol)
	cp	'Z'
	ret	z
	inc	a
	ld	(curLocCol), a
	ret

command_print:
	call	command_memdump
	ld	hl, youtyped_msg
	call	print
	ld	hl, inputbuf
	call	print
	ret

command_pgup:
	or	a	; clear carry flag
	ld	de, memdump_page_size * cols_to_dump
	ld	hl, (memdumpStart)
	sbc	hl, de
	ld	(memdumpStart), hl
	call	command_memdump
	ret

command_pgdn:
	ld	de, memdump_page_size * cols_to_dump
	ld	hl, (memdumpStart)
	add	hl, de
	ld	(memdumpStart), hl
	call	command_memdump
	ret

command_memdump:
	ld	hl, (memdumpStart)
	call memdump
	ret	
	