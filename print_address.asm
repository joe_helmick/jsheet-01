;---------------------------------------------------------------------------------------------------
print_address:
	; save registers
	push	bc
	push	hl
	; push hl because we'l need it again to print next byte 
	push 	hl
	; print MSB first
	ld	a, h
	call	bin_to_hex
	ld	a, h	
	rst 8
	ld	a, l
	rst 8
	; pop hl to get LSB
	pop	hl
	; print LSB next
	ld	a, l
	call	bin_to_hex 
	ld	a, h	
	rst 8
	ld	a, l
	rst 8
	; print a : and a space to separate output 
	ld	a, ':'
	rst 8
	ld	a, ' '
	rst 8
	; restore registers
	pop	hl
	pop	bc
	ret
