;ORG zero from z88dk appmake command line, ORG = 0x0000 where this first reset vector belongs.

hwreset:
	di			; turn off interrupts until started up
	jp	initialize
;---------------------------------------------------------------------------------------------------
	defs	4, 0		; placeholder to align the vector to correct address
reset0008:
	jp	txa
;---------------------------------------------------------------------------------------------------
	defs 	45, 0x00	; placeholder to align the vector to correct address
reset0038:
	jr	serialinterrupt
;---------------------------------------------------------------------------------------------------
serialinterrupt:

	push	af
	push	hl
	in	a, ($80)
	and	$01		; is read buffer full?
	jr	z, rts0		; ignore if not
	in	a, ($81)	; read status
	push	af
	ld	a,(serbufused)
	cp	serbufsz	; ignore if full
	jr	nz, notfull
	pop	af
	jr	rts0
notfull:
	ld	hl,(serinptr)
	inc	hl
	ld	a,l
	cp	serfull & $ff
	jr	nz,notwrap
	ld	hl,serbuf
notwrap:
	ld	(serinptr),hl
	pop	af
	ld	(hl),a
	ld	a,(serbufused)
	inc	a
	ld	(serbufused),a
	cp	serfulsz
	jr	c,rts0
	ld	a,rts_high
	out	($80),a
rts0:
	pop	hl
	pop	af
	ei
	reti
	
;---------------------------------------------------------------------------------------------------
rxa:
waitforchar:
	ld	a, (serbufused)
	cp	$00
	jr	z, waitforchar
	push	hl
	ld	hl, (serrdptr)
	inc	hl
	ld	a,l
	cp	serfull & $ff
	jr	nz, notrdwrap
	ld	hl, serbuf
notrdwrap:
	di
	ld	(serrdptr), hl
	ld	a, (serbufused)
	dec	a
	ld	(serbufused), a
	cp	serempsz
	jr	nc, rts1
	ld	a, rts_low
	out	($80), a
rts1:
	ld	a, (hl)
	ei
	pop	hl
	ret			; char ready in a
	
;---------------------------------------------------------------------------------------------------
txa:
	push	af		; store char in a for later
conout:	in	a, ($80)	; read acia status byte       
	bit	1, a		; set z flag if still transmitting character       
	jr	z, conout	; and loop until flag signals ready
	pop	af		; get the character back from the stack
	out	($81), a	; send it over rs232 to acia output address
	ret
	
;---------------------------------------------------------------------------------------------------
initialize:

	; zero out all RAM first  DEBUG ONLY, shouldn't be necessary in production
	ld	hl, ramstart
	ld	bc, 65536-ramstart
	ld	a, 0
	ld	(hl), a
	ld	d, h
	ld	e, l
	inc	de
	dec	bc
	ld	a, b
	or	c
	ret	z
	ldir			; loop unti done zeroing RAM
	
	;----------------------------------------------------------------

	ld	hl, serbuf	; set pointer to serial buffer
	ld	(serinptr), hl	; set tx pointer to same location
	ld	(serrdptr), hl	; set rx pointer to same location
	xor	a		; set a to zero
	ld	(serbufused), a	; set buffer used amount to zero
	ld	a, rts_low	; load the acia configuration byte...
	out	($80), a	; and initialize the acia with it
	im	1		; set interrupt mode...
	ei			; and enable interrupts
	jp	main1		; jump to start of program
