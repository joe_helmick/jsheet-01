;------------------------------------------------------------------------------------------
; Memory Fill
; From "Z80 Assembly Language Subroutines" 1983 p. 196
; input		HL = base address to start fill
; input		BC = length of memory to fill
; input		A = value to fill memory with

mfill:
	ld	(hl), a
	ld	d, h
	ld	e, l
	inc	de
	dec	bc
	ld	a, b
	or	c
	ret	z
	ldir
	ret
